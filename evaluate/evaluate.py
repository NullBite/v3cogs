from redbot.core import commands
from redbot.core import checks

class Evaluate(commands.Cog):
    """My custom cog"""

    @commands.command()
    async def mycom(self, ctx):
        """This does stuff!"""
        # Your code will go here
        await ctx.send("I can do stuff!")

    @commands.command()
    @checks.is_owner()
    async def restricted(self, ctx):
        """Ownership test"""
        await ctx.send("User is owner")
